package com.hychen39.restuful_test.facades;

import com.hychen39.restuful_test.TicketCareDBClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by steven on 12/4/2017.
 */
public class ReceiverFacadeTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void findInterest() throws Exception {
        String interestsURI = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.receiver/{id}/interests";
        ReceiverFacade receiverFacade = new ReceiverFacade(new TicketCareDBClient());
        receiverFacade.setReceiverInterestURI(interestsURI);
        String result = receiverFacade.findInterest(5501);
        assertNotNull(result);
    }

}