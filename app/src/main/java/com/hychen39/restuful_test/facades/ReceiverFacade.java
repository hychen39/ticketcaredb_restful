package com.hychen39.restuful_test.facades;

import android.util.Log;

import com.hychen39.restuful_test.TicketCareDBClient;

import org.json.JSONObject;

/** The facade class to access Receiver information by using RESTful web service.
 * Created by steven on 12/4/2017.
 */

public class ReceiverFacade {
    /**
     * Find the receiver's interest.
     * @param receiverID
     * @return
     */
    private TicketCareDBClient wsClient;

    private String receiverURI;
    private String receiverInterestURI;

    public TicketCareDBClient getWsClient() {
        return wsClient;
    }

    public void setWsClient(TicketCareDBClient wsClient) {
        this.wsClient = wsClient;
    }

    public String getReceiverURI() {
        return receiverURI;
    }

    public void setReceiverURI(String receiverURI) {
        this.receiverURI = receiverURI;
    }

    public String getReceiverInterestURI() {
        return receiverInterestURI;
    }

    /**
     *
     * @param receiverInterestURI URI should follow the format:
     *                            "http://machine:port/TicketCareDB/webresources/entities.receiver/{id}/interests";
     */
    public void setReceiverInterestURI(String receiverInterestURI) {
        this.receiverInterestURI = receiverInterestURI;
    }

    public ReceiverFacade(TicketCareDBClient wsClient) {
        this.wsClient = wsClient;
    }

    /**
     * Set the {@link #receiverInterestURI} before using the method.
     * @param receiverID
     * @return String of the JSON format
     */
    public String findInterest(int receiverID){
        String finalURI = this.receiverInterestURI.replace("{id}", String.valueOf(receiverID));
        Log.d("findInterest >>", "Real URI: " + finalURI);
        String r = wsClient.doGetRequest(finalURI);
        return r;
    }
}
