package com.hychen39.restuful_test;

import android.content.Context;
import android.os.AsyncTask;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        TicketCareDBClient client = new TicketCareDBClient();
        // execute doInBackground()
        client.execute();
        while (client.getStatus() == AsyncTask.Status.RUNNING){
            Log.i("mycode", "Background Process running...");
        }
        if (client.getStatus() == AsyncTask.Status.FINISHED) {
            JSONArray postArray = client.getBodyContentJSONArray();
            Log.i("mycode", postArray.toString());
            assertNotEquals(0, postArray.length());
        }
        //assertEquals("com.hychen39.restuful_test", appContext.getPackageName());
    }
}
