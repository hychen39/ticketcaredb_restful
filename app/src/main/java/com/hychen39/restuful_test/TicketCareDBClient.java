package com.hychen39.restuful_test;

import android.os.AsyncTask;
import android.util.Log;

import com.hychen39.restuful_test.domains.TicketCategoryEntity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by steven on 11/2/2017.
 */

public class TicketCareDBClient extends AsyncTask<String, Integer, Integer> {
    public static final String POST_RES_URL = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.post";
    public static final String TICKET_CATEGORY_RES_URL = "http://163.17.9.161:8080/TicketCareDB/webresources/entities.ticketcategory";
    private InputStreamReader _bodyReader;
    private HttpURLConnection _conn = null;
    private String _bodyContent;

    /**
     * Do the HTTP Get request.
     *
     * @param urlStr RESTful Web Service URL
     * @return the JSON format string
     */
    public String doGetRequest(String urlStr) {
        URL targetURL = null;
        String returnValue = null;
        try {
            targetURL = new URL(urlStr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpURLConnection conn;
        try {
            conn = (HttpURLConnection) targetURL.openConnection();
// prepare request header
            conn.setRequestProperty("User-Agent", "Android_TicketCareDBClient");
            conn.setRequestProperty("Accept", "application/json");
            conn.connect();

            int statusCode = conn.getResponseCode();
            Log.d("doGetRequest >> Status Code:", ""+statusCode);
            if (statusCode == 200){
                InputStreamReader isr = new InputStreamReader(conn.getInputStream());
                returnValue = this.getBodyContent(isr).get(0);
                isr.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return returnValue;
    }

    /**
     * 取得 Response 的 Input Stream (Using UTF-8 Endoding)並放到 {@link #_bodyReader}中.
     *
     * @return InputStreamReader for the connection.
     */
    public InputStreamReader getResponseStreamReader() {
        //Create url object
        URL postResUrl = null;
        try {
            postResUrl = new URL(POST_RES_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        // Create connection

        try {
            _conn = (HttpURLConnection) postResUrl.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // prepare request header
        _conn.setRequestProperty("User-Agent", "Android_TicketCareDBClient");
        _conn.setRequestProperty("Accept", "application/json");
        // connect

        // get response
        int responseCode = 0;
        try {
            _conn.connect();
            responseCode = _conn.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (responseCode == 200) {
            try {
                _bodyReader = new InputStreamReader(_conn.getInputStream(), "UTF-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return _bodyReader;
    }


    /**
     * Read the data from the input stream of the Response. Close the stream after completion.
     *
     * @param reader InputStreamReader from the connection
     * @return Contents from the InputStreamReader.
     */
    public List<String> getBodyContent(InputStreamReader reader) {
        List<String> lines = new ArrayList<>();
        BufferedReader bufferReader = new BufferedReader(reader);
        String line;
        try {
            while ((line = bufferReader.readLine()) != null) {
                lines.add(line);
            }
            bufferReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public InputStreamReader get_bodyReader() {
        return _bodyReader;
    }

    /**
     * Print the Post entity. Get the ID and the ticket address from the Post object.
     *
     * @param jsonObject Post entity object represented by the JSON format.
     * @return
     * @deprecated Use {@link com.hychen39.restuful_test.domains.PostEntity#printPostObject(JSONObject)} instead.
     */
    public String printPostObject(JSONObject jsonObject) {
        String result = null;
        String formatStr = "%s \t %s \n";
        StringBuilder sb = new StringBuilder();
        int id = 0;
        String address = null;
        Formatter formatter = new Formatter();
        try {
            id = jsonObject.getInt("id");
        } catch (JSONException e) {
            //e.printStackTrace();
            System.out.print("No ID for this post. \n");
        }

        try {
            address = jsonObject.getJSONObject("ticketId").getString("address");
        } catch (JSONException e) {
            System.out.print("No Address for this post. \n");
        }

        formatter.format(formatStr, id, address);
        result = formatter.toString();

        return result;
    }

    /**
     * The backgound process to connect to web service and get the response body.
     * The response content is put in {@link #_bodyContent}
     */
    public void run() {
        List<String> contents = this.getBodyContent(this.getResponseStreamReader());
        _bodyContent = contents.get(0);
    }


    /**
     * Convert {@link #_bodyContent} to JSONArray.
     * Get the body content before calling this method.
     *
     * @return
     * @see {@link #getResponseStreamReader()}, {@link #getBodyContent(InputStreamReader)}
     */
    public JSONArray getBodyContentJSONArray() {
        JSONArray jsonArray = null;

        try {
            jsonArray = new JSONArray(_bodyContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    @Override
    protected Integer doInBackground(String... strings) {
        this.run();
        return null;
    }


    @Override
    protected void onPostExecute(Integer integer) {
//        super.onPostExecute(integer);
        // print out the result
        JSONArray posts = getBodyContentJSONArray();
        Log.i("mycode", posts.toString());
    }

    /**
     * Post TicketCategory 到 Web Service. 此方法必須在 {@link #doInBackground(String...)} 中執行.
     *
     * @param ticketCategory
     * @see com.hychen39.restuful_test.domains.TicketCategoryEntity
     */
    public String postTicketCategory(TicketCategoryEntity ticketCategory) {

        // Prepare the JSON object
        JSONObject jsonObject = ticketCategory.toJsonObject();

        URL postURL = null;
        try {
            postURL = new URL(TICKET_CATEGORY_RES_URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        String status = "", msg = "";
        try {
            // open connection
            HttpURLConnection conn = (HttpURLConnection) postURL.openConnection();
            // set request method and properties
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.connect();
            // get the output stream
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            // write the content
            os.writeBytes(jsonObject.toString());
            // flush and close output stream
            os.flush();
            os.close();
            // log response
            status = String.valueOf(conn.getResponseCode());
            msg = conn.getResponseMessage();
            Log.i("STATUS", status);
            Log.i("MSG", msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // return status code.
        return status;

    }
}
