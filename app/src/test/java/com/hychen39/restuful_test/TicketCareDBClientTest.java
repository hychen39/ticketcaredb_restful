package com.hychen39.restuful_test;

import com.hychen39.restuful_test.domains.TicketCategoryEntity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by steven on 11/30/2017.
 */
public class TicketCareDBClientTest {
    @Test
    public void doGetRequest() throws Exception {
        // Create the DB client
        TicketCareDBClient client = new TicketCareDBClient();
        String result = client.doGetRequest(TicketCareDBClient.POST_RES_URL);
        assertNotNull(result);
    }

    @Test
    public void postTicketCategory() throws Exception {
        // Create the DB client
        TicketCareDBClient client = new TicketCareDBClient();
        //Create a new ticket category entity
        TicketCategoryEntity ticketCategory = new TicketCategoryEntity();
        ticketCategory.setId(new Long(11));
        ticketCategory.setName("PostedCategory-11");
        //
        String status = client.postTicketCategory(ticketCategory);
        assertEquals("204", status);
    }

}