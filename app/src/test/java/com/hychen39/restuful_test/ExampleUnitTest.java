package com.hychen39.restuful_test;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        TicketCareDBClient ticketCareDBClient = new TicketCareDBClient();
//        List<String> messages = ticketCareDBClient.getResult();
//        assertNotEquals(0, messages.size());
//        ticketCareDBClient.getResponseStreamReader();
        List<String> streamContent = ticketCareDBClient.getBodyContent(ticketCareDBClient.getResponseStreamReader());
        assertNotEquals(0, streamContent.size());
        String jsonDoc = streamContent.get(0);
        // ref: https://stackoverflow.com/questions/3951274/how-to-unit-test-json-parsing
        // use Object Model API to consume JSON document
        JSONArray jsonArray = new JSONArray(jsonDoc);
//        System.out.print(jsonArray.toString());
        for (int i=0; i < jsonArray.length(); i++){
            JSONObject o = jsonArray.getJSONObject(i);
            // print out the content of the json object
            if (o != null)
                 System.out.print(ticketCareDBClient.printPostObject(o) + "\n");
        }

    }
}